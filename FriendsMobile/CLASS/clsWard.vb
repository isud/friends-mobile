﻿Imports FriendsMobile

Public Class clsWard
    Implements iWardRoom




    Public Function getRoomWard(wardid As Integer) As DataTable Implements iWardRoom.getRoomWard
        Dim sql As String

        sql = "SELECT r.bedsname,rs.`status` AS `Status`,f.hn AS HN "
        sql += ",CONCAT_WS('',CASE WHEN(p.`name` = '') THEN CONCAT_WS('',masprename.`seprename`,' ',p.`ename`) ELSE CONCAT_WS('',masprename.`ftprename`,' ',p.`name`) end,'  ',CASE WHEN(p.`lname` = '') THEN p.`elname` ELSE CONCAT_WS('',p.`lname`) end) AS `Name`,f.f_lock "
        sql += ",r.bedsid,f.vn,f.an,s.shid , CONCAT_WS('',masprename1.stprename,' ',hospemp.name,' ' ,hospemp.`lname`) AS doctorname ,  CONCAT(`addresdec1`,'  ',CASE WHEN(`tambon` IS null) THEN '' ELSE CONCAT('ต.',' ',`tambon`) END,'  ',CASE WHEN(`ampur` IS null) THEN '' ELSE CONCAT('อ.',' ',`ampur`) END,'  ',CASE WHEN(`changwat` IS null) THEN '' ELSE CONCAT('จ.',' ',`changwat`) end, '  ',CASE WHEN(mastambon.`postcode` IS null) THEN '' ELSE CONCAT(mastambon.`postcode`) END) AS 'address' "
        sql += "FROM (SELECT clinic,bedsid,bedsname,stid FROM sroomitem WHERE clinic = '" & wardid & "'  AND `stid` <> 11) AS r "
        sql += "LEFT JOIN masrmstatus AS rs ON r.stid = rs.stid "
        sql += "LEFT JOIN (SELECT an,bedsid,shid FROM frnshift WHERE status = 1) AS s ON r.bedsid = s.bedsid "
        sql += "LEFT JOIN (SELECT * FROM frnadmission WHERE datetime_disch IS NULL ) AS f ON s.an = f.an "
        sql += "LEFT JOIN (SELECT hn,`name`,lname,penname,prename,ename,elname from  person ) AS p on f.hn = p.hn "
        sql += "LEFT JOIN masprename ON p.prename = masprename.prename "
        sql += "LEFT JOIN (SELECT * FROM maspenname WHERE `status` = 1) AS penname ON f.penid = penname.penid "
        sql += "LEFT JOIN (SELECT * FROM hospemp WHERE `f_dr` = 1) AS hospemp ON hospemp.empid = f.dr "
        sql += "LEFT JOIN (SELECT * FROM masprename WHERE `status` = 1) AS masprename1 ON masprename1.prename = hospemp.prename "
        sql += " LEFT JOIN  (SELECT * FROM address WHERE  `addresstype` = 1  AND status = 1) AS address  ON address.hn = p.hn "
        sql += "LEFT JOIN maschangwat AS maschangwat ON address.`codechangwat` = maschangwat.`codechangwat` "
        sql += "LEFT JOIN masampur AS masampur ON address.`ampid` = masampur.`ampid` "
        sql += "LEFT JOIN mastambon AS mastambon ON address.`tmbid` = mastambon.`tmbid` "
        sql += "ORDER BY r.bedsname;"
        Return condb.GetTable(sql)
    End Function
End Class
