﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="footer.ascx.vb" Inherits="FriendsMobile.footer" %>

  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        <i>Develop a passion for learning. If you do, You will never cease to grow.</i>
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2016 <a href="#">FRIENDS HIS</a>.</strong> All rights reserved.
  </footer>