﻿Imports DevExpress.Web

Public Class Ward01
    Inherits System.Web.UI.Page
    Dim w As New icWardRoom


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        w._Iward = New clsWard
        w._setRoomList = 18
        w._iPerson = New clsPerson
        w._iVitalSign = New clsPersonVitalSign


        'MsgBox(w._iPerson.abogroupdesc)
        ASPxGridView1.DataBind()
        gridVitalSign.DataBind()


    End Sub

    Protected Sub grid_DataBinding(sender As Object, e As EventArgs) Handles ASPxGridView1.DataBinding
        ASPxGridView1.DataSource = w._getRoomList

    End Sub


    Private Sub ASPxGridView1_FocusedRowChanged(sender As Object, e As EventArgs) Handles ASPxGridView1.FocusedRowChanged
        Dim detgrid As ASPxGridView = TryCast(sender, ASPxGridView)

        Dim value As Object = detgrid.GetDataRow(detgrid.FocusedRowIndex)
        w._An = value("an")
        w.getVitasign()
        '  txtHn2.Text = value.ToString

    End Sub

    Protected Sub gridVitalSign_DataBinding(sender As Object, e As EventArgs) Handles gridVitalSign.DataBinding
        w._An = "5908060013"
        w.getVitasign()

        gridVitalSign.DataSource = w._VitalSign

    End Sub
End Class