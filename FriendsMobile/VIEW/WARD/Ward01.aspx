﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Ward01.aspx.vb" Inherits="FriendsMobile.Ward01" %>

<%@ Register assembly="DevExpress.Web.v15.1, Version=15.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
     <script type="text/javascript">
        var postponedCallbackRequired = false;
        function OnListBoxIndexChanged(s, e) {
            if(CallbackPanel.InCallback())
                postponedCallbackRequired = true;
            else
                CallbackPanel.PerformCallback();
        }
        function OnEndCallback(s, e) {
            if(postponedCallbackRequired) {
                CallbackPanel.PerformCallback();
                postponedCallbackRequired = false;
            }
        } 
    </script>
      <script type="text/javascript">
        function OnGridFocusedRowChanged() {
            // Query the server for the "EmployeeID" and "Notes" fields from the focused row 
            // The values will be returned to the OnGetRowValues() function
          //  DetailNotes.SetText("Loading...");
            ASPxGridView1.GetRowValues(ASPxGridView1.GetFocusedRowIndex(), 'HN;Name;Status;bedsname;address', OnGetRowValues);
        }
        // Value array contains "EmployeeID" and "Notes" field values returned from the server 
        function OnGetRowValues(values) { 
            //DetailImage.SetImageUrl("FocusedRow.aspx?Photo=" + values[0]);
           //txtHn.SetVisible(true);
            txtHn.SetText(values[0]);
            txtName.SetText(values[1])
            txtBedRoom.SetText(values[3])
            txtAddress.SetText(values[4])
        }
    </script>

<body>
    <form id="form1" runat="server">
    <div>
    
        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" KeyFieldName="HN" Width="100%" EnableCallBacks="False">
            <SettingsBehavior AllowSelectSingleRowOnly="True" ProcessSelectionChangedOnServer="True"  AllowFocusedRow="True" />
            <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
            <Columns>
                <dx:GridViewDataTextColumn FieldName="HN" VisibleIndex="1">
                </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn FieldName="an" VisibleIndex="2" UnboundType="Integer">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Name" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                   <dx:GridViewDataTextColumn FieldName="bedsname" VisibleIndex="4"  >
                </dx:GridViewDataTextColumn>
                     <dx:GridViewDataTextColumn FieldName="address" VisibleIndex="5"  Visible="False">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="doctorname" VisibleIndex="6"  >
                </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn FieldName="Status" VisibleIndex="7" Visible="False">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataButtonEditColumn VisibleIndex="8">
                    <PropertiesButtonEdit DisplayFormatInEditMode="True">
                        <Buttons>
                            <dx:EditButton Text="dasdas" Width="50px">
                                <Image IconID="actions_apply_32x32">
                                </Image>
                            </dx:EditButton>
                        </Buttons>
                    </PropertiesButtonEdit>
                </dx:GridViewDataButtonEditColumn>
            </Columns>

      <Settings ShowGroupPanel="true" />
        <SettingsBehavior AllowFocusedRow="True" />
        <ClientSideEvents FocusedRowChanged="function(s, e) { OnGridFocusedRowChanged(); }" />

        </dx:ASPxGridView>
    
    </div>
        <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="HN">
        </dx:ASPxLabel>
        <dx:ASPxTextBox ID="txtHn" runat="server" Width="170px">
        </dx:ASPxTextBox>
        <dx:ASPxTextBox ID="txtName" runat="server" Width="170px">
        </dx:ASPxTextBox>
        <dx:ASPxTextBox ID="txtBedRoom" runat="server" Width="170px">
        </dx:ASPxTextBox>
        <dx:ASPxMemo ID="txtAddress" runat="server" Height="71px" Width="170px">
        </dx:ASPxMemo>
        <dx:ASPxTextBox ID="txtHn2" runat="server" Width="170px">
        </dx:ASPxTextBox>
        <dx:ASPxGridView ID="gridVitalSign" runat="server"  AutoGenerateColumns="False" KeyFieldName="vsid" Width="100%" EnableCallBacks="False" >

             <SettingsBehavior AllowSelectSingleRowOnly="True" ProcessSelectionChangedOnServer="True"  AllowFocusedRow="True" />
            <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
            <Columns>
                <dx:GridViewDataTextColumn FieldName="vsid" VisibleIndex="1">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="vsdesc" VisibleIndex="2">
                </dx:GridViewDataTextColumn>
                   <dx:GridViewDataTextColumn FieldName="value" VisibleIndex="3"  >
                </dx:GridViewDataTextColumn>
                     <dx:GridViewDataTextColumn FieldName="date" VisibleIndex="4"  Visible="true">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="name" VisibleIndex="5"  >
                </dx:GridViewDataTextColumn>
                 <dx:GridViewDataTextColumn FieldName="min" VisibleIndex="6" Visible="true">
                </dx:GridViewDataTextColumn>
                  <dx:GridViewDataTextColumn FieldName="max" VisibleIndex="6" Visible="true">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataButtonEditColumn VisibleIndex="7">
                    <PropertiesButtonEdit DisplayFormatInEditMode="True">
                        <Buttons>
                            <dx:EditButton Text="dasdas" Width="50px">
                                <Image IconID="actions_apply_32x32">
                                </Image>
                            </dx:EditButton>
                        </Buttons>
                    </PropertiesButtonEdit>
                </dx:GridViewDataButtonEditColumn>
            </Columns>

      <Settings ShowGroupPanel="true" />
        <SettingsBehavior AllowFocusedRow="True" />
        <ClientSideEvents FocusedRowChanged="function(s, e) { OnGridFocusedRowChanged(); }" />



        </dx:ASPxGridView>
    </form>
</body>
</html>
