﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports FriendsMobile

Public Class clsPerson
    Implements iPersonDataAN


    Implements iPersonDataVn
    Private _abogroupdesc As String
    Private _hn As String
    Private _name As String
    Public Property hn As String
        Get
            Return _hn
        End Get
        Set(value As String)
            _hn = value
        End Set
    End Property

    Public Property cid As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property prename As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property stprename As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property name As String
        Get
            Return _name
        End Get
        Set(value As String)

        End Set
    End Property

    Public Property lname As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property ename As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property emname As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property elname As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property penname As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property birth As DateTime
        Get
            Return Nothing
        End Get
        Set(value As DateTime)
        End Set
    End Property

    Public Property sex As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property sexdesc As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property race As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property nation As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property religion As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property nationdesc As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property racedesc As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property religiondesc As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property abogroup As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property rhgroup As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property vn As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property an As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property bedsid As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property bedsname As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property abogroupdesc As String
        Get
            Return _abogroupdesc
        End Get
        Set(value As String)
            _abogroupdesc = value
        End Set
    End Property


    Private Function getPersonByan(an As Integer) As Boolean
        Dim sql As String
        sql = "SELECT  name ,lname "
        sql += "   "
        sql += ",h.penid "
        sql += "FROM (SELECT an,hn,penid FROM  frnadmission WHERE an = '" & vn & "') AS h  "
        sql += "LEFT JOIN  person AS p ON h.hn = p.hn "
        sql += "LEFT JOIN  masprename AS pre ON p.prename = pre.prename "
        sql += "LEFT JOIN (SELECT * FROM  maspenname WHERE `status` = 1) AS penname ON h.penid = penname.penid "

        Dim dt As New DataTable
        dt = condb.GetTable(sql)
        If dt.Rows.Count > 0 Then
            hn = dt.Rows(0)("hn").ToString
            name = dt.Rows(0)("name").ToString
            lname = dt.Rows(0)("lname").ToString
            cid = dt.Rows(0)("cid").ToString
            prename = dt.Rows(0)("prename").ToString
            stprename = dt.Rows(0)("stprename").ToString

            Return True
        Else
            Return False
        End If


    End Function

    Private Function iPersonData_getPersonDataAN(an As Integer) As Boolean Implements iPersonDataAN.getPersonData
        Return getPersonByan(an)
    End Function
    Private Function iPersonData_getPersonDataVN(vn As Integer) As Boolean Implements iPersonDataVn.getPersonData

    End Function
End Class
