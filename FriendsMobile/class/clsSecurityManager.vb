﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel


Public Class clsSecurityManager
    Private _usreq As String
    Private _provider As String
    Private _resultLogin As Boolean
    Private _iUserDoctor As iUserDoctor
    Private _iUser As iUser


    Public Property usreq As String
        Get
            Return _usreq
        End Get
        Set(value As String)
            _usreq = value
        End Set
    End Property

    Public Property provider As String
        Get
            Return _provider
        End Get
        Set(value As String)
            _provider = value
        End Set
    End Property

    Public Property iUserDoctor As iUserDoctor
        Get
            Return _iUserDoctor
        End Get
        Set(value As iUserDoctor)
            _iUserDoctor = value
        End Set
    End Property

    Public Property iUser As iUser
        Get
            Return _iUser
        End Get
        Set(value As iUser)
            _iUser = value
        End Set
    End Property

    Public Sub processAuthenticationDoctor(Optional ByRef txtLoginWarning As DevExpress.Web.ASPxLabel = Nothing)
        Dim objAuthen As New clsUserDB
        _resultLogin = objAuthen.searchUser(_provider, _usreq)

        If _resultLogin = True Then
            System.Web.HttpContext.Current.Session("empid") = objAuthen.empid
            System.Web.HttpContext.Current.Session("f_dr") = objAuthen.f_dr
            System.Web.HttpContext.Current.Session("empName") = objAuthen.empName
            System.Web.HttpContext.Current.Response.Redirect("VIEW/DASHBOARD/dashboard.aspx")
        Else
            System.Web.HttpContext.Current.Session.Clear()

            If txtLoginWarning IsNot Nothing Then
                txtLoginWarning.Text = "รหัสผ่านไม่ถูกต้อง!"
            End If

            'System.Web.HttpContext.Current.Response.Redirect("index.aspx")
        End If
    End Sub

    Public Sub processAuthenticationUser()
        _resultLogin = _iUser.searchUser(_usreq)

        If _resultLogin = True Then

        Else
            System.Web.HttpContext.Current.Session.Clear()
        End If
    End Sub
End Class
