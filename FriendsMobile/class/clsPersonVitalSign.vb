﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports FriendsMobile

Public Class clsPersonVitalSign
    Inherits clsPerson
    Implements iPersonVitalSign

    Private _an As String
    Private _vn As String
    Private _dtVitalsign As New DataTable


    Public Overloads Property an As String Implements iPersonVitalSign.an
        Get
            Return _an
        End Get
        Set(value As String)
            _an = value
        End Set
    End Property

    Public Overloads Property vn As String Implements iPersonVitalSign.vn
        Get
            Return _vn
        End Get
        Set(value As String)
            _vn = value
        End Set
    End Property


    Public Property dtVitalsign As DataTable Implements iPersonVitalSign.dtVitalsign
        Get
            Return _dtVitalsign
        End Get
        Set(value As DataTable)
            _dtVitalsign = value
        End Set
    End Property

    Private Function getLastVitalsign() Implements iPersonVitalSign.getlastVitalSign

        Dim sql As String
        sql = "SELECT d_update FROM frnvsopd WHERE an = '" & an & "' ORDER BY mvsid DESC LIMIT 1;"
        Dim dt As New DataTable
        dt = condb.GetTable(sql)

        If dt.Rows.Count > 0 Then
            sql = "SELECT mvs.vsid,mvs.vsdesc,fvs.resultvs AS `value`,fvs.d_update AS `date`,e.`name`,fvs.mvsid,mvs.`min`,mvs.`max`,mvs.ll,mvs.hl "
            sql += "FROM (SELECT * "
            sql += "FROM (SELECT * FROM frnvsopd WHERE an = '" & an & "' AND d_update  = '" & Convert.ToDateTime(dt.Rows(0)("d_update")).Year & Convert.ToDateTime(dt.Rows(0)("d_update")).ToString("-MM-dd HH:mm:ss") & "' ORDER BY vsid, d_update DESC, resultvs) AS x "
            sql += "GROUP BY vsid "
            sql += ") AS fvs "
            sql += "RIGHT JOIN (SELECT * FROM masvstype WHERE `status` = 1 AND f_ipd = 1) AS mvs ON fvs.vsid = mvs.vsid "
            sql += "LEFT JOIN (SELECT empid,CONCAT_WS('',`name`,' ',lname) AS `name` FROM hospemp) AS e ON fvs.usmk = e.empid "
            sql += "ORDER BY mvs.lstorder"
            condb.GetTable(sql, dtVitalsign)

        End If

    End Function

End Class
