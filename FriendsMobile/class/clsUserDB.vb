﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports FriendsMobile
Imports FriendsMobile.Module1

Public Class clsUserDB
    Implements iUserDoctor, iUser

    Private _empid As String
    Private _empName As String
    Private _f_dr As Boolean

    Public Property empid As String Implements iUserDoctor.empid, iUser.empid
        Get
            Return _empid
        End Get
        Set(value As String)
            _empid = value
        End Set
    End Property

    Public Property empName As String
        Get
            Return _empName
        End Get
        Set(value As String)
            _empName = value
        End Set
    End Property

    Public Property f_dr As Boolean
        Get
            Return _f_dr
        End Get
        Set(value As Boolean)
            _f_dr = value
        End Set
    End Property

    Public Function searchUser(provider As String, usreq As String) As Boolean Implements iUserDoctor.searchUser
        Dim dt As New DataTable
        condb = clsConnectDB.NewConnection
        Dim sql As String
        sql = "SELECT * FROM  hospemp WHERE provider='" & provider & "';"
        dt = condb.GetTable(sql)
        If dt.Rows.Count > 0 Then
            If dt.Rows(0).Item("usreq") = usreq Then
                _empid = dt.Rows(0)("empid")
                _f_dr = dt.Rows(0)("f_dr")
                _empName = dt.Rows(0)("name") & " " & dt.Rows(0)("lname")

                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function

    Public Function searchUser(usreq As String) As Boolean Implements iUser.searchUser
        Dim dt As New DataTable
        condb = clsConnectDB.NewConnection
        Dim sql As String
        sql = "SELECT * FROM  hospemp WHERE usreq='" & usreq & "';"
        dt = condb.GetTable(sql)
        If dt.Rows.Count > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
End Class
