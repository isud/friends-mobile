﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports FriendsMobile
Imports FriendsMobile.Module1

Public Class clsOPDPatient
    Implements iPatientOfDoc

    Private _drusmk As String

    Public Property dtDoctorQueue As DataTable
        Get
            Return dtsetUserDB.Tables("dtDoctorQue")
        End Get
        Set(value As DataTable)

        End Set
    End Property

    Private Property drusmk As String Implements iPatientOfDoc.drusmk
        Get
            Return _drusmk
        End Get
        Set(value As String)
            _drusmk = value
        End Set
    End Property

    Public Sub getDoctorQueue()
        condb = clsConnectDB.NewConnection

        Dim sql As String = "SELECT c.`clinicid` AS 'CLINICID',CAST(h.hn AS CHAR(15)) AS HN,CASE WHEN (h.penid  = 0) THEN CONCAT(p.`name`,' ',p.lname) ELSE CONCAT(penname.`penname`,' ', penname.`penlname`)  END   AS `NAME`,cstatus ,h.vn AS 'VN' , c.bclinic AS 'BCLINIC' FROM (SELECT penid,vn,hn FROM   frnservice WHERE f_discharge = 1 ) AS h INNER JOIN (SELECT * FROM  frnclinic WHERE clinic = (SELECT clinic FROM  hospemp WHERE empid = '" & _drusmk & "') AND cstatus IN (1,2,3,4) AND doctor ='" & _drusmk & "' ) AS c ON h.vn = c.vn JOIN  person AS p ON h.hn = p.hn LEFT JOIN (SELECT * FROM  maspenname WHERE `status` = 1) AS penname ON h.penid = penname.penid ORDER BY c.sentime ASC;"
        condb.GetTable(sql, dtsetUserDB.Tables("dtDoctorQue"))

    End Sub
End Class
