﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel

Public Class clsProduct
    Public Property prdcode As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property prdname As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property genericname As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property code As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property prdcat As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property catname As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property ccode As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property rcode As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property iccode As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property ircode As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property hccode As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property hrcode As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property b_unit As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property s_unit As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property b_unitname As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property s_unitname As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property avg_cost As Double
        Get
            Return Nothing
        End Get
        Set(value As Double)
        End Set
    End Property

    Public Property last_cost As Double
        Get
            Return Nothing
        End Get
        Set(value As Double)
        End Set
    End Property

    Public Property opdprc As Double
        Get
            Return Nothing
        End Get
        Set(value As Double)
        End Set
    End Property

    Public Property ipdprc As Double
        Get
            Return Nothing
        End Get
        Set(value As Double)
        End Set
    End Property

    Public Property status As Boolean
        Get
            Return Nothing
        End Get
        Set(value As Boolean)
        End Set
    End Property

    Public Property f_stk As Boolean
        Get
            Return Nothing
        End Get
        Set(value As Boolean)
        End Set
    End Property

    Public Property f_lot As Boolean
        Get
            Return Nothing
        End Get
        Set(value As Boolean)
        End Set
    End Property

    Public Property f_overspend As Boolean
        Get
            Return Nothing
        End Get
        Set(value As Boolean)
        End Set
    End Property

    Public Property last_avgcost As Double
        Get
            Return Nothing
        End Get
        Set(value As Double)
        End Set
    End Property

    Public Property f_reuse As Boolean
        Get
            Return Nothing
        End Get
        Set(value As Boolean)
        End Set
    End Property
End Class
