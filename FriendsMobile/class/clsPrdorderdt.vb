﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel

Public Class clsPrdorderdt
    Public Property id As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property orderid As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property hn As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property vn As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property an As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property prdcode As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property prdname As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property chkin_date As DateTime
        Get
            Return Nothing
        End Get
        Set(value As DateTime)
        End Set
    End Property

    Public Property chkin_provider As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property chkin_providername As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property prdprc As Double
        Get
            Return Nothing
        End Get
        Set(value As Double)
        End Set
    End Property

    Public Property drprc As Integer
        Get
            Return Nothing
        End Get
        Set(value As Integer)
        End Set
    End Property

    Public Property qty As Double
        Get
            Return Nothing
        End Get
        Set(value As Double)
        End Set
    End Property

    Public Property mqty As Double
        Get
            Return Nothing
        End Get
        Set(value As Double)
        End Set
    End Property

    Public Property prdcat As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property catname As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property f_chkin As Boolean
        Get
            Return Nothing
        End Get
        Set(value As Boolean)
        End Set
    End Property

    Public Property f_comp As Boolean
        Get
            Return Nothing
        End Get
        Set(value As Boolean)
        End Set
    End Property

    Public Property f_cancel As Boolean
        Get
            Return Nothing
        End Get
        Set(value As Boolean)
        End Set
    End Property

    Public Property ord_status As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property ord_statusname As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property ccode As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property rcode As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property label As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property label_en As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property caution As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property caution_en As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property unitid As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property unitidname As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property lotid As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property usmk As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property

    Public Property usmktime As DateTime
        Get
            Return Nothing
        End Get
        Set(value As DateTime)
        End Set
    End Property

    Public Property remark As String
        Get
            Return Nothing
        End Get
        Set(value As String)
        End Set
    End Property
End Class
