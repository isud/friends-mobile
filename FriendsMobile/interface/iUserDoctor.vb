﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel

Public Interface iUserDoctor

    Property empid As String

    Function searchUser(provider As String, usreq As String) As Boolean

End Interface
